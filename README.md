# go-serialization #

Go databases are a great source of data for testing and for training.
This project provides access to Scala (and Java) object graph representations of the files in those databases.  
(In service of my long-term goal of constructing an automated Go player.)

Some of these resources:
  * [Andries Brouwer's database](http://homepages.cwi.nl/~aeb/go/games/index.html) seems to grow each week.
  * [Byheart Go](https://sites.google.com/site/byheartgo/) also has many games.

### Getting started ###

Scala code snippet for using this project:  

    val sgfSource = io.Source.fromFile(sgfFilename).getLines.mkString("\n")  
    val collection = SGFParser.parse(sgf)  
    val primaryGame = collection.iterator.next
    // get the number of moves in the first game in the collection
    val numMoves = primaryGame.path.filter(n: Node => n.moveProps.size > 0).size
    // get the name of the black player, or an exception if the property isn't in the expected location
    val blackPlayer = primaryGame.path(0).textProps.get(PropIdent("PB")).text

On my machine, this project parses the **8,880 games** from the two sites above, containing **1,857,272 moves** in **2.5 minutes**.

==========

The grammar for the SGF format can be found [here](http://www.red-bean.com/sgf/sgf4.html), the class breakdown of the parsed model comes directly from the grammar found there.


Selected SGF file concepts:
  * `Collection` -- a sequence of `GameTree`s
  * `GameTree` -- a sequence of `Node`s common to several games, followed by branches, the moves specific to each game.
    * `Collection`s and `GameTree`s are for storing multiple games, or position explorations, in a single file.
  * `Node` -- a sequence of (prop-id: prop-value) mappings, i.e. the result of the game (which side won), or which point was taken on a particular move
  * Primitive property types -- `Move`, `Point`, `Color`, `Text`, `Number`, `Real`, `Triple`

### A bit more than just parsing ###

  * The model classes have `.toSGF` methods, so you can build a model, and store off the SGF very easily.
  * SGF is extensible, so allows room for properties not defined initially, but many properties are predefined.  For predefined properties, this parser parses them to the proper type.
  * The object graph produced by the parser is mostly just data, but a few classes contain some convenience functionality:
    * The `Collection.iterator` method returns an iterater over `GamePath` instances.  Each `GamePath` is a list of `Node`s representing a complete game.
      * This just elides the complexities of collections, and tree structures.
    * The `Node` class organizes properties in a typesafe way.
    * `PropIdent.description` holds a textual description.
