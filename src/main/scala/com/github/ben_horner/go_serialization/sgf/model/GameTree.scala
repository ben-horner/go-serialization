package com.github.ben_horner.go_serialization.sgf.model

case class GameTree(nodes: List[Node], branches: List[GameTree]) {
  
  require(nodes.size >= 1, "attempted to create GameTree() with empty Node list")
  
  def toSGF: String = "(" + nodes.map(_.toSGF).mkString(";", ";", "") + branches.map(_.toSGF).mkString + ")"
  
  def allPaths: Iterator[List[GameTree]] = {
    if (branches.isEmpty) Iterator(List(this))
    else branches.iterator.flatMap(_.allPaths.map(this :: _))
  }
  
}
