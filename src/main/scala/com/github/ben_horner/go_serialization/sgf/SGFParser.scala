package com.github.ben_horner.go_serialization.sgf

import scala.util.parsing.combinator._
import com.github.ben_horner.go_serialization.sgf.model._
import scala.None
import com.github.ben_horner.go_serialization.sgf.model.Point
import org.apache.log4j.Logger

object SGFParser extends JavaTokenParsers {

  val logger = Logger.getLogger("SGFParser")

  // from: http://www.red-bean.com/sgf/ff1_3/ff3.html

  // Collection	::==	{GameTree}.
  // GameTree 	::==	"(" Sequence {GameTree} ")".
  // Sequence	::==	Node {Node}.
  // Node		::==	";" {Property}.

  // Property	::==	PropIdent PropValue {PropValue}.
  // PropIdent	::==	UpperCase [UpperCase | Digit].
  // PropValue	::==	"[" [Number | Text | Real | Triple | Color | Move | Point | ...] "]".

  // Number		::==	["+"|"-"] Digit {Digit}.
  // Text		::==	{ any character; "\]"="]", "\\"="\"}.
  // Real		::==	Number ["." {Digit}].
  // Triple		::==	("1" | "2"). 
  // Color		::==	("B" | "W").

  
  // My version (distilled from other information there, but oriented on parsing to the correct types)

  // Collection	::==	{GameTree}.
  // GameTree 	::==	"(" Sequence {GameTree} ")".
  // Sequence	::==	Node {Node}.
  // Node		::==	";" {Property}.

  // Property		::==	PredefinedProperty | NewProperty.
  // NewProperty	::==	NewPropIdent TextPropValue.
  // NewPropIdent	::==	UpperCase [UpperCase | Digit].
  // PropValue		::==	MoveListPropValue | PointListPropValue |
  //						EmptyPropValue | MovePropValue | PointPropValue |
  //						NumberPropValue | TextPropValue | RealPropValue |
  //						TriplePropValue | ColorPropValue.
  // ...

  def parse(s: String): Collection = {
    val r = filterNonPropValues(s)

    // TODO:  Once this initial filtering of prop ids is correct, we can delete this logging...
    val original = augmentString(s).filter(c => c != '\n' && c != '\r' && c != ' ')
    val replaced = augmentString(r).filter(c => c != '\n' && c != '\r' && c != ' ')
    if (original != replaced) logger.debug("PropIdents changed!" + "\noriginal: " + original + "\nreplaced: " + replaced)

    parseAll(collection, r).get
  }

  override val skipWhitespace = false

  def collection: Parser[Collection] = rep(gameTree)<~white                         ^^ (Collection(_))
  def gameTree  : Parser[GameTree]   = white~"("~>sequence~rep(gameTree)<~white~")" ^^ { case ns~gts => GameTree(ns, gts) }
  def sequence  : Parser[List[Node]] = node~rep(node)                               ^^ { case n~ns => n :: ns }
  def node      : Parser[Node]       = white~";"~rep(property)                      ^^ { case ignore1~";"~props => Node(props.toMap) }

  def property          : Parser[(PropIdent, Any)] = (predefinedProperty | newProperty)
  def predefinedProperty: Parser[(PropIdent, Any)] = (moveListProperty | pointListProperty | textListProperty |
                                                      moveProperty /* | pointProperty*/ | textProperty |
                                                      realProperty | numberProperty | tripleProperty |
                                                      colorProperty | emptyProperty)
  def newProperty : Parser[(PropIdent, Text)] = white~>newPropIdent~textPropValue ^^ { case id~value => PropIdent(id) -> value }
  def newPropIdent: Parser[String]            = upperCase~rep(upperCase | digit)  ^^ { case d~ds => d + ds.mkString }

  def moveListProperty : Parser[(PropIdent, List[Move])]  = white~>moveListPropIdent~moveListPropValue   ^^ { case id~value => PropIdent(id) -> value }
  def pointListProperty: Parser[(PropIdent, List[Point])] = white~>pointListPropIdent~pointListPropValue ^^ { case id~value => PropIdent(id) -> value }
  def textListProperty : Parser[(PropIdent, List[Text])]  = white~>textListPropIdent~textListPropValue   ^^ { case id~value => PropIdent(id) -> value }
  def moveProperty     : Parser[(PropIdent, Move)]        = white~>movePropIdent~movePropValue           ^^ { case id~value => PropIdent(id) -> value }
//  def pointProperty    : Parser[(PropIdent, Point)]       = white~>pointPropIdent~pointPropValue         ^^ { case id~value => PropIdent(id) -> value }
  def textProperty     : Parser[(PropIdent, Text)]        = white~>textPropIdent~textPropValue           ^^ { case id~value => PropIdent(id) -> value }
  def realProperty     : Parser[(PropIdent, Real)]        = white~>realPropIdent~realPropValue           ^^ { case id~value => PropIdent(id) -> value }
  def numberProperty   : Parser[(PropIdent, Number)]      = white~>numberPropIdent~numberPropValue       ^^ { case id~value => PropIdent(id) -> value }
  def tripleProperty   : Parser[(PropIdent, Triple)]      = white~>triplePropIdent~triplePropValue       ^^ { case id~value => PropIdent(id) -> value }
  def colorProperty    : Parser[(PropIdent, Color)]       = white~>colorPropIdent~colorPropValue         ^^ { case id~value => PropIdent(id) -> value }
  def emptyProperty    : Parser[(PropIdent, Any)]         = white~>emptyPropIdent~emptyPropValue         ^^ { case id~value => PropIdent(id) -> None }

  def moveListPropIdent : Parser[String] = "SE"
  def pointListPropIdent: Parser[String] = ("AB" | "AW" | "AE" | "SL" | "MA" | "TR" | "CR" | "TB" | "TW" | "SC" | "RG")
  def textListPropIdent : Parser[String] = "LB"
  def movePropIdent     : Parser[String] = ("B" | "W")
//  def pointPropIdent    : Parser[String] = ("")
  def textPropIdent     : Parser[String] = ("C" | "N" | "RU" | "DG" | "GN" | "GC" | "EV" | "RO" | "DT" | "PC" | "PB" | "PW" |
                                            "BT" | "WT" | "RE" | "US" | "TM" | "SO" | "AN" | "CP" | "ID" | "ON" | "BR" | "WR")
  def realPropIdent     : Parser[String] = ("V" | "BL" | "WL" | "OP" | "KM")
  def numberPropIdent   : Parser[String] = ("OB" | "OM" | "OV" | "OW" | "MN" | "FF" | "GM" | "SZ" | "BS" | "WS" | "HA" | "TC")
  def triplePropIdent   : Parser[String] = ("CH" | "GB" | "GW" | "TE" | "BM" | "UC" | "DM" | "HO" | "SI")
  def colorPropIdent    : Parser[String] = ("PL")
  def emptyPropIdent    : Parser[String] = ("KO" | "DO" | "IT" | "FG" | "LT")

  def moveListPropValue : Parser[List[Move]]  = rep(movePropValue)
  def pointListPropValue: Parser[List[Point]] = rep(pointPropValue)
  def textListPropValue : Parser[List[Text]]  = rep(textPropValue)
  def movePropValue     : Parser[Move]        = white~"["~>move<~"]"
  def pointPropValue    : Parser[Point]       = white~"["~>point<~"]"
  def textPropValue     : Parser[Text]        = white~"["~>text<~"]"
  def realPropValue     : Parser[Real]        = white~"["~>real<~"]"
  def numberPropValue   : Parser[Number]      = white~"["~>number<~"]"
  def triplePropValue   : Parser[Triple]      = white~"["~>triple<~"]"
  def colorPropValue    : Parser[Color]       = white~"["~>color<~"]"
  def emptyPropValue    : Parser[Any]         = white~"[]"~white ^^ (_ => None)

  def move : Parser[Move] = (point | "tt" | "") ^^ {
    _ match {
      case p: Point => Move(Some(p))
      case s: String => Move(None)
    }
  }
  def point    : Parser[Point] = file~rank                  ^^ { case file~rank => Point(file + rank) }
  def text     : Parser[Text]  = rep("""\\""" | """\]""" | """[^\]]""".r) ^^ { case s => Text(s.mkString) } // { any character; "\]"="]", "\\"="\"}.
  def real     : Parser[Real]  = number~opt("."~rep(digit)) ^^ {
    case Number(whole)~decOpt => decOpt match {
      case None => Real(whole)
      case Some("."~decDigs) => Real(whole + "." + decDigs.mkString)
    }
  }
  def number   : Parser[Number] = opt("+" | "-")~digit~rep(digit) ^^ {
    case sign~d~ds => Number(
      if (sign == Some("-")) "-" + d + ds.mkString
      else d + ds.mkString
    )
  }
  def triple   : Parser[Triple] = ("1" | "2") ^^ (Triple(_))
  def color    : Parser[Color]  = ("B" | "W") ^^ (Color(_))
  def file     : Parser[String] = index
  def rank     : Parser[String] = index
  def index    : Parser[String] = ("a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" | "j" | "k" | "l" | "m" | "n" | "o" | "p" | "q" | "r" | "s")
  def upperCase: Parser[String] = ("A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" | "J" | "K" | "L" | "M" |
                                   "N" | "O" | "P" | "Q" | "R" | "S" | "T" | "U" | "V" | "W" | "X" | "Y" | "Z")
  def digit    : Parser[String] = ("0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9")
  def white    : Parser[String] = """\s*""".r

  //  def timeProperty     : Parser[(PropIdent, Real)]        = white~>timePropIdent~timePropValue             ^^ { case id~value => PropIdent(id) -> value }
  //  def timePropIdent		: Parser[String] = ("BL" | "WL" | "OP")
  //  def timePropValue  : Parser[Real]   = white~"["~>time<~"]"

  //  val numRgx = """\d*(?:\.\d*)?"""
  //  val hrsRgx = """(?:(""" + numRgx + """)\s*hr?)"""
  //  val minRgx = """(?:(""" + numRgx + """)\s*m)"""
  //  val secRgx = """(""" + numRgx + """)"""
  //  val hmsRgx = (hrsRgx + """?\s*""" + minRgx + """?\s*""" + secRgx + """?""").r
  //
  //  def timeStringToSeconds(timeString: String): Double = {
  //    val hmsRgx(hrsStr, minStr, secStr) = timeString
  //    val hoursAsSeconds   = if ((hrsStr == null) || (hrsStr == "")) 0 else 60 * 60 * hrsStr.toDouble
  //    val minutesAsSeconds = if ((minStr == null) || (minStr == "")) 0 else 60 * minStr.toDouble
  //    val secondsAsSeconds = if ((secStr == null) || (secStr == "")) 0 else secStr.toDouble
  //    hoursAsSeconds + minutesAsSeconds + secondsAsSeconds
  //  }
  //
  //  def time  : Parser[Real]   = hmsRgx ^^ (hmsStr => Real(timeStringToSeconds(hmsStr).toString))

  
  
  // filterNonPropValues() is a bit messy...
  // The alternative was to try to capture all possible property id's, then rewrite them in their canonical form.
  // Since many property ids are captured separately in order to infer correct types, that would make the grammar stuff above
  // very messy.  I chose to isolate the mess here, rather than spread it to the grammar, or to give up the type capturing.
  
  // * Only upper case letters are significant in a property ID.  (others are ignored as stated above)
  // * Outside of [] have to ignore everything not in {UpperCase, '(', ')', ';', '[', ']'}
  // * Primarily, property names need to be filtered down to at most upper+upper, or upper+digit -- newer SGF files have longer ids...
  def filterNonPropValues(s: String): String = {
    val structuralChars = Set('(', ')', ';', '[', ']')
    def keepers(c: Char) = c.isUpper || c.isDigit || structuralChars.contains(c)

    val sb = new StringBuilder
    var span = s.span(c => c != '[') // entering prop value (span._1 is entirely outside prop value, span._2 is inside)
    var outside: String = span._1
    var remainder: String = span._2
    while (!remainder.isEmpty) {
      sb.append(augmentString(outside).filter(keepers))
//      logger.debug("appended outside: \""+ outside.filter(keepers) + "\"; remainder = "+ remainder.size)
      span = remainder.span(c => c != ']') // potentially exiting prop value
      var inside: String = span._1
      remainder = span._2
      while (inside.endsWith("""\""")) { // ah!  it was escaped
        inside = inside + remainder.substring(0, 1)
        remainder = remainder.substring(1)
        sb.append(inside)
//        logger.debug("appended inside: \""+ inside +"\"; remainder = "+ remainder.size)
        span = remainder.span(c => c != ']')
        inside = span._1
        remainder = span._2
      } // now finally exiting prop value
      inside = inside + remainder.substring(0, 1)
      remainder = remainder.substring(1)
      sb.append(inside)
//      logger.debug("appended inside: \""+ inside +"\"; remainder = "+ remainder.size)
      span = remainder.span(c => c != '[')
      outside = span._1
      remainder = span._2
    }
    sb.append(augmentString(outside).filter(keepers))
    sb.toString
  }
  
}
