package com.github.ben_horner.go_serialization.sgf.model

case class PropIdent(id: String) {
  require((1 <= id.length) && (id.length <= 5) && id.charAt(0).isUpper &&
    ((id.length == 1) || id.charAt(1).isUpper || id.charAt(1).isDigit), "attempted creating PropIdent(" + id + ")")
  def toSGF: String = id
  lazy val description = PropIdent.propertyDescriptions(id)
}

object PropIdent {
  val propertyDescriptions: Map[String, String] = Map(
    "FF" -> "File format",
    "GM" -> "Game type (Go = 1, Othello = 2, chess = 3, Gomoku+Renju = 4, Nine Men's Morris = 5, 6: not used, Chinese chess = 7, Shogi = 8)",

    "EV" -> "Event (tournament)",
    "GN" -> "Game name",
    "ID" -> "The game ID",
    "PC" -> "Place",
    "RO" -> "Round in tournament",
    "DT" -> "Date",
    "SO" -> "source (book, journal, ...)",
    "GC" -> "Game comment",
    "US" -> "User: who entered game",

    "PB" -> "Black player name",
    "BR" -> "Black's Rank",
    "BS" -> "black species (human=0, computer>0)",
    "BT" -> "Team of black player",
    "PW" -> "White player name",
    "WR" -> "White's Rank",
    "WS" -> "white species (human=0, computer>0)",
    "WT" -> "Team of white player",

    "AN" -> "Who analyzed the game",
    "CP" -> "Copyright on game comments",
    "N" -> "Node name",
    "C" -> "Comment",

    "SZ" -> "size of the board",
    "HA" -> "handicap",
    "KM" -> "komi",
    "RU" -> """Rules: "Japanese", "Chinese", "Ing", ..., default is "Japanese"""",
    "TM" -> "time limit per player",

    "V" -> "Node value",
    "CH" -> "Check mark",
    "GB" -> "Good for black",
    "GW" -> "Good for white",
    "TE" -> "Good move (tesuji)",
    "BM" -> "bad move",
    "DO" -> "doubtful move",
    "IT" -> "interesting move",
    "UC" -> "Unclear position",
    "DM" -> "even position",
    "HO" -> "Hotspot node mark",
    "SI" -> "Position marked with a sigma",

    "BL" -> "Time left for Black, seconds",
    "WL" -> "Time left for White, seconds",
    "OB" -> "Number of black stones to play in this byo-yomi period",
    "OW" -> "Number of white stones to play in this byo-yomi period",
    "OM" -> "Number of moves for each overtime period",
    "OP" -> "Length of each overtime period",
    "OV" -> "seconds of operator overhead for each move in byo-yomi",

    "B" -> "Black move",
    "W" -> "White move",

    "LT" -> "Lose on Time is enforced",
    "RE" -> "Result of the game:",

    "AB" -> "Edit: Add black stones",
    "AW" -> "Edit: Add white stones",
    "AE" -> "Edit: add empty points (= remove stones)",
    "PL" -> "Player whose turn it is, 1 = Black, 2 = White",

    "DG" -> "Diagram, for printing",
    "FG" -> "figure, for printing",
    "MN" -> "Set the current move number to number provided, used for diagrams",
    "SL" -> "selected points",
    "MA" -> "points marked with cross",
    "TR" -> "points marked with triangles",
    "CR" -> "Circle marker",
    "LB" -> "labels on points",
    "TB" -> "Black territory",
    "TW" -> "White territory",
    "TC" -> "Territory count: B-W",
    "SC" -> "Secure stones",
    "RG" -> "region of the board",

    "SE" -> "Moves tried so far in self-test mode",
    "KO" -> "WinKo, allows to execute the current move even if it is illegal.",
    "ON" -> "The opening played").withDefault(_ => "")

  val sgfProps = Set("FF", "GM").map(PropIdent(_))
  val gameInfoProps = Set("EV", "GN", "ID", "PC", "RO", "DT", "SO", "GC", "US").map(PropIdent(_))
  val playerInfoProps = Set("PB", "BR", "BS", "BT", "PW", "WR", "WS", "WT").map(PropIdent(_))
  val commentatorProps = Set("AN", "CP", "N", "C").map(PropIdent(_))
  val ruleProps = Set("SZ", "HA", "KM", "RU", "TM").map(PropIdent(_))

  val timeControlProps = Set("BL", "WL", "OB", "OW", "OM", "OP", "OV").map(PropIdent(_))

  val moveProps = Set("B", "W").map(PropIdent(_))
  val moveAnnotationProps = Set("V", "CH", "GB", "GW", "TE", "BM", "DO", "IT", "UC", "DM", "HO", "SI").map(PropIdent(_))
  val resultProps = Set("LT", "RE").map(PropIdent(_))

  val setupProps = Set("AB", "AW", "AE", "PL").map(PropIdent(_))
  val diagramProps = Set("DG", "FG", "MN", "SL", "MA", "TR", "CR", "LB", "TB", "TW", "TC", "SC", "RG").map(PropIdent(_))

  val randomProps = Set("SE", "KO", "ON").map(PropIdent(_))

}
