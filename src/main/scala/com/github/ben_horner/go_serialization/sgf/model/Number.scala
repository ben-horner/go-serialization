package com.github.ben_horner.go_serialization.sgf.model

object Number {
  val signChars = Set('+', '-')
}

case class Number(number: String) extends PropValue {
  require(number.forall(c => c.isDigit || Number.signChars.contains(c)), "attempted to create Number(" + number + ")")
  val toInt = number.toInt
  def toSGF: String = number
}
