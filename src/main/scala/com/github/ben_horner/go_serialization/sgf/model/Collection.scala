package com.github.ben_horner.go_serialization.sgf.model

import scala.collection.immutable.Iterable
import com.github.ben_horner.go_serialization.sgf.GamePath

case class Collection(gameTrees: List[GameTree]) extends Iterable[GamePath] {
  
  def toSGF: String = gameTrees.map(_.toSGF).mkString
  
  def treePaths: Iterator[List[GameTree]] = gameTrees.iterator.flatMap(_.allPaths)
  
  def iterator: Iterator[GamePath] = treePaths.map(_.flatMap(_.nodes)).map(GamePath(_))
  
}
