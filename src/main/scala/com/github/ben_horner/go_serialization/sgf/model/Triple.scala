package com.github.ben_horner.go_serialization.sgf.model

case class Triple(triple: String) extends PropValue {
  require((triple == "1") || (triple == "2"), "attempted creating Triple(" + triple + ")")
  def toSGF: String = triple
}
