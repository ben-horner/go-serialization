package com.github.ben_horner.go_serialization.sgf

import com.github.ben_horner.go_serialization.sgf.model.Node
import com.github.ben_horner.go_serialization.sgf.model.PropIdent
import com.github.ben_horner.go_serialization.sgf.model.Move
import com.github.ben_horner.go_serialization.sgf.model.Color
import com.github.ben_horner.go_serialization.sgf.model.Point

object GamePath {
  val blackMove = PropIdent("B")
  val whiteMove = PropIdent("W")
  val addBlack = PropIdent("AB")
  val addWhite = PropIdent("AW")
  val playerToMove = PropIdent("PL")
  val gameResult = PropIdent("RE")
}

case class MoveNode(node: Node, pre: List[Node], post: List[Node]) {
  require(node.moveProps.size == 1)
  val color: Color = Color(node.moveProps.keys.head.id)
  val point: Option[Point] = node.moveProps.values.head.move
}

case class GamePath(path: List[Node]) {

  import GamePath._
  
  lazy val moveNodes: Seq[MoveNode] = {
    def noMoves(node: Node) = node.properties.filterKeys(PropIdent.moveProps).isEmpty
    var result: Seq[MoveNode] = Seq()
    
    var span = path.span(noMoves)
    var pre = span._1
    while(!span._2.isEmpty){
      val node = span._2.head
      span = span._2.tail.span(noMoves)
      val post = span._1
      result = result :+ MoveNode(node, pre, post)
      pre = post
    }
    
    result
  }
  
}