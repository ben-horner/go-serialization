package com.github.ben_horner.go_serialization.sgf.model

import scala.None

case class Node(properties: Map[PropIdent, Any]) {

  def mkPropSGF(property: (PropIdent, Any), propIdToString: (String => String) = (x => x)) = {
    property match {
      case (id, l: List[PropValue]) => propIdToString(id.toSGF) + l.map(_.toSGF).mkString("[", "][", "]")
      case (id, None) => propIdToString(id.toSGF) + "[]"
      case (id, propValue: PropValue) => propIdToString(id.toSGF) + "[" + propValue.toSGF + "]"
    }
  }
  
  def moveListProps: Map[PropIdent, List[Move]] = for((pi, pv: List[Move]) <- properties) yield (pi -> pv)
  def pointListProps: Map[PropIdent, List[Point]] = for((pi, pv: List[Point]) <- properties) yield (pi -> pv)
  def textListProps: Map[PropIdent, List[Text]] = for((pi, pv: List[Text]) <- properties) yield (pi -> pv)
  def moveProps: Map[PropIdent, Move] = for((pi, pv: Move) <- properties) yield (pi -> pv) 
  def pointProps: Map[PropIdent, Point] = for((pi, pv: Point) <- properties) yield (pi -> pv)
  def textProps: Map[PropIdent, Text] = for((pi, pv: Text) <- properties) yield (pi -> pv)
  def realProps: Map[PropIdent, Real] = for((pi, pv: Real) <- properties) yield (pi -> pv)
  def numberProps: Map[PropIdent, Number] = for((pi, pv: Number) <- properties) yield (pi -> pv)
  def tripleProps: Map[PropIdent, Triple] = for((pi, pv: Triple) <- properties) yield (pi -> pv)
  def colorProps: Map[PropIdent, Color] = for((pi, pv: Color) <- properties) yield (pi -> pv)
  
  def noValProps: Set[PropIdent] = {
    properties.filter {
      case (id, value) => value match {
        case None => true
        case _ => false
      }
    }.map { case (id, _) => (id) }.toSet
  }

  def toSGF: String = properties.map(mkPropSGF(_)).mkString("")

}
