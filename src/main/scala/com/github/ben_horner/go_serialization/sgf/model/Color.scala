package com.github.ben_horner.go_serialization.sgf.model

case class Color(color: String) extends PropValue {
  require((color == "B") || (color == "W"), "attempted creating Color(" + color + ")")
  def toSGF: String = color
}
