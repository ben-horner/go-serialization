package com.github.ben_horner.go_serialization.sgf.model

case class Real(real: String) extends PropValue {
  val double = real.toDouble
  def toSGF: String = real
}
