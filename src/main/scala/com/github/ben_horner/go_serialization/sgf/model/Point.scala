package com.github.ben_horner.go_serialization.sgf.model

object Point {
  
  def apply(point: String): Point = {
    require(point.length == 2, "attempted creating Point("+ point +")")
    Point(point(0), point(1))
  }
    
  implicit val orderByFileThenRank = new Ordering[Point]{
    override def compare(x: Point, y: Point): Int = {
      var diff = x.file - y.file
      if (diff == 0)
        diff = x.rank - y.rank
      diff
    }
  }

}

case class Point(val file: Char, val rank: Char) extends PropValue {
  require((file >='a') && (file <= 's') && (rank >= 'a') && (rank <= 's'), "attempted creating Point(" + file + "," + rank + ")")
  
  def toSGF: String = "" + file + rank
  override def toString: String = toSGF
  
}
