package com.github.ben_horner.go_serialization.sgf.model

case class Text(text: String) extends PropValue {
  def toSGF: String = text
  override def toString: String = text
}
