package com.github.ben_horner.go_serialization.sgf.model

trait PropValue {
  def toSGF: String
}