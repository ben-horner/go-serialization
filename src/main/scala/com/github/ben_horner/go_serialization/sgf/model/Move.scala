package com.github.ben_horner.go_serialization.sgf.model

case class Move(move: Option[Point]) extends PropValue {
  
  def toSGF: String = move match {
    case None => "tt"
    case Some(p) => p.toSGF
  }
  
  override def toString: String = move match {
    case None => "pass"
    case Some(p) => p.toString
  }
  
}
