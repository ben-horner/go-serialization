package com.github.ben_horner.go_serialization.sgf

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.apache.log4j.Logger
import java.io.File
import com.github.ben_horner.go_serialization.sgf.model.Collection
import com.github.ben_horner.go_serialization.sgf.model.GameTree
import com.github.ben_horner.go_serialization.sgf.model.PropIdent

object ParseEverything {

  val logger = Logger.getLogger("ParseEverything")

  def main(args: Array[String]){
    val games = new File("""../go/go-serialization/src/test/resources/go""")
    
    var nameToCollection: Map[String, Collection] = Map()
    var couldNotParse: Seq[String] = Seq()
    def recurse(f: File) {
      if (f.isDirectory()) {
        f.listFiles().foreach(recurse(_))
      } else if (f.getName.endsWith(".sgf")) {
        try {
          val src = io.Source.fromFile(f)
          val s: String = src.toList.mkString
          src.close
          val collection = SGFParser.parse(s)
          logger.debug("parsed [" + f.getName + "]: " + collection.toSGF)
          nameToCollection = nameToCollection + (f.getAbsolutePath -> collection)
        } catch {
          case e: Exception => {
            couldNotParse = couldNotParse :+ f.getAbsolutePath
            logger.error("couldn't parse " + f.getName, e)
          }
        }
      }
    }

    recurse(games)

    var propIdentToCount: Map[PropIdent, Int] = Map()
    nameToCollection.foreach {
      case (name, Collection(gameTrees)) =>
        def recurse(gameTree: GameTree) {
          val GameTree(nodes, branches) = gameTree
          branches.foreach(recurse(_))
          nodes.foreach {
            _.properties.keySet.foreach(propIdent =>
              propIdentToCount = propIdentToCount + (propIdent -> (1 + propIdentToCount.getOrElse(propIdent, 0))))
          }
        }
        gameTrees.foreach(recurse)
    }

    logger.debug("collections parsed: " + nameToCollection.size)
    logger.debug("properties actually used:")
    propIdentToCount.toList.sortBy(_._2).foreach {
      case (propIdent, count) =>
        logger.debug(propIdent.id + " (" + count + "):\t" + propIdent.description)
    }

    logger.debug("Could not parse SGF files: ")
    logger.debug(couldNotParse.mkString("\n"))
  }

}