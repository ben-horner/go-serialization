package com.github.ben_horner.go_serialization.sgf.model

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class NodeTest extends FunSuite {
  
  test("Node.typedProps function actually separates by types...") {
    val node = Node(Map(PropIdent("FF") -> Number("4"), (PropIdent("B") -> Move(Some(Point("jj"))))))
    
    
    
    assert(node.moveProps.size == 1)
  }
  
}